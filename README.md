[![pipeline status](https://git.coop/aptivate/ansible-roles/lets-encrypt/badges/master/pipeline.svg)](https://git.coop/aptivate/ansible-roles/lets-encrypt/commits/master)

# lets-encrypt

A role to install and configure HTTPS certificates.

## How does it work?

Here's the steps:

  * Setup a dummy HTTP configuration for your domain.
  * Run the certificate generation.
  * Setup the cronjob to renew the certificates.
  * Delete the temporary HTTP configuration.

You can then go on to specify the default location of the certificates:

> /etc/letsencrypt/live/{{ letsencrypt_domain }}/

In your configuration files after this role is run.

That's It!

# Requirements

  * A working web server installation.
    * Check [aptivate/ansible-roles] for roles to use.

  * A DNS entry that resolves.

[aptivate/ansible-roles]: https://git.coop/aptivate/ansible-roles

# Role Variables

## Mandatory

  * `letsencrypt_domain`: The domain to generate certificates for.

  * `letsencrypt_email`: The contract email address for your certificates.

## With Defaults

  * `using_apache`: Generate certificates with the Apache plugin.
    * Default is `true`.

  * `apache_conf_dir`: The location of the Apache configuration.
    * Default is `/etc/httpd`

  * `apache_service_name`: The name of the Apache service for `systemd` calls.
    * Default is `httpd`. Useful for SCL managed Apache.

# Dependencies

  * A role that installs a web server.

# Example Playbook

```yaml
- hosts: localhost
  roles:
     - role: apache-httpd
     - role: lets-encrypt
       letsencrypt_domain: foobar.aptivate.org
       letsencrypt_email: "carers+letsencrypt@aptivate.org"
```

# Testing

We use Linode for our testing here.

You'll need to expose the `LINODE_API_KEY` environment variable for that.


```bash
$ pipenv install --dev
$ pipenv run molecule test
```

# License

  * https://www.gnu.org/licenses/gpl-3.0.en.html

# Author Information

  * https://aptivate.org/
  * https://git.coop/aptivate
