def test_certbot_package_installed(host):
    certbot_server_package = host.package('python2-certbot-apache')

    assert certbot_server_package.is_installed


def test_certbot_cronjob_in_place(host):
    cmd = host.run('crontab -l')

    assert cmd.rc == 0
    assert 'certbot --apache renew' in cmd.stdout.strip()
